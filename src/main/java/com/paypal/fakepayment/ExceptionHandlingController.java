package com.paypal.fakepayment;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@ControllerAdvice

public class ExceptionHandlingController extends ResponseEntityExceptionHandler  {

	@ExceptionHandler(PaymentSystemException.class)
	public ResponseEntity<Map<String, Object>> appError(PaymentSystemException exc) {

		Map<String, Object> toRet = new HashMap<>();
		toRet.put("message", exc.getMessage());
		toRet.put("code", HttpStatus.CONFLICT);
		toRet.put("timestamp", new Date().getTime());

		return ResponseEntity.status(HttpStatus.CONFLICT).body(toRet);
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<Map<String, Object>> noFound (NoSuchElementException exc) {
		Map<String, Object> toRet = new HashMap<>();
		toRet.put("message", exc.getMessage());
		toRet.put("code", HttpStatus.NOT_FOUND);

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(toRet);
	}

	
	

}