package com.paypal.fakepayment.impl.user;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;

@RestController
@RequestMapping("api/fakepay/user")
@AllArgsConstructor

public class UserController {

	private final UserManagerService userManagerService;

	@PostMapping("create")
	public PaymentSystemUser createUser(@RequestBody CreateUserDto data) throws PaymentSystemException {
		return userManagerService.createUser(data.getFirstName(), data.getLastName(), data.getEmailAddress());
	}
	
	
	@GetMapping("list") 
	@ApiOperation(value = "users list ")
	public List<User> listUsers(){
		return userManagerService.listAll();
	}

	@Data
	private static final class CreateUserDto {
		private String firstName;
		private String lastName;
		private String emailAddress;
	}

}
