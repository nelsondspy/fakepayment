package com.paypal.fakepayment.impl.user;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.PaymentSystemUserManager;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserManagerService  implements PaymentSystemUserManager{
	private final UserRepository repository  ;

	@Override
	public PaymentSystemUser createUser(String firstName, String lastName, String emailAddress)
			throws PaymentSystemException {
		if (repository.findByEmailAddress(emailAddress).isPresent())
			  throw new PaymentSystemException("user was already created: "+ emailAddress); 
			
		User user = new User(firstName, lastName);
		user.setEmailAddress(emailAddress);
		return repository.save(user);
	}
	
	
	public User getUserByEmail(String emailAddress) {
		Optional<User> user = repository.findByEmailAddress(emailAddress);
		user.orElseThrow(() -> new NoSuchElementException("emailaddress not found:" + emailAddress));
		return user.get() ;
	}
	
	public User save(User user) {
		return repository.save(user);
	}
	

	public List<User >listAll() {
		return repository.findAll();
	}
	
	

}
