package com.paypal.fakepayment.impl.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.impl.account.Account;

import lombok.Data;
import lombok.NonNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Data
@Table(name = "\"user\"")
public class User implements PaymentSystemUser  {
	
	@Id
	@GeneratedValue
	@JsonIgnore
	private long id;

	private String firstName;
	private String lastName;
	
	/**
	 * Identifies each user in the system  
	 */
	@NonNull	
	private String emailAddress; 
	
	/**
	 * A user can only be linked to 1 account  for the sake of simplicity .
	 */
	@OneToOne
	@JsonIgnore
	private Account account;
	
	public  User (String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
		
	}
	public User() {}
	
	
}
