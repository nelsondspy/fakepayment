package com.paypal.fakepayment.impl.account;

import java.util.Iterator;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;

@RestController
@RequestMapping("api/fakepay/account")
@AllArgsConstructor
public class AccountController {
	private final AccountManagerService service;
	
	@PostMapping("create")
	@ApiOperation(value = "Create a new account, user must be created previously")
	public void createAccount(@RequestBody AccountCreateDto data) throws PaymentSystemException {
		service.createAccount(data.getEmailAddress(), data.getInitialBalance());
		
	}

	
	@PutMapping("add/user")
	@ApiOperation(value = "Asing an pre-existent account to a pre-existent user")
	public PaymentSystemAccount addUserToAccount(@RequestBody UserAddAccountDto data) throws PaymentSystemException {
		return service.addUserToAccount(data.getEmailAddress(), data.getAccountNumber());
	}	
	
	
	@GetMapping("list")
	@ApiOperation(value = "list all accounts")
	public Iterator<PaymentSystemAccount> getAllAccounts() {
		return  service.getAllAccounts();
	}
	
	
	@GetMapping("firstname")
	@ApiOperation(value = "search account using users firtsname")
	public Iterator<PaymentSystemAccount> getListName(String name) {
		return  service.findAccountsByFirstName(name);
	}


	@Data
	private static final class AccountCreateDto{
		private String emailAddress;
		private long initialBalance;
	}
	
	@Data
	private static final class UserAddAccountDto{
		private String emailAddress;
		private String accountNumber;
	}  
	 
}
