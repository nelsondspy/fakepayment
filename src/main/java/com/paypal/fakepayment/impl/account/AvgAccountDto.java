package com.paypal.fakepayment.impl.account;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AvgAccountDto {
	private String accountNumber;
	private long usersCount;
	private double averageUser;
}
