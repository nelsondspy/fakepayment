package com.paypal.fakepayment.impl.account;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemAccountManager;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.impl.user.User;
import com.paypal.fakepayment.impl.user.UserManagerService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AccountManagerService implements PaymentSystemAccountManager {
	private final AccountRepository accountRepository;
	private final UserManagerService userService;

	private static final int LENGTH_ACCOUNT = 5;

	@Override
	public PaymentSystemAccount createAccount(PaymentSystemUser user, double initialBalance)
			throws PaymentSystemException {

		String accountNumber = RandomStringUtils.randomAlphanumeric(LENGTH_ACCOUNT);
		Account account = new Account();
		account.setAccountNumber(accountNumber);
		account.setAccountBalance(initialBalance);

		Account saved = accountRepository.save(account);
		User userIns = ((User) user);
		userIns.setAccount(saved);
		userService.save(userIns);
		return saved;
	}

	public PaymentSystemAccount createAccount(String email, double initialBalance) throws PaymentSystemException {

		return createAccount(userService.getUserByEmail(email), initialBalance);
	}

	@Override
	public PaymentSystemAccount addUserToAccount(PaymentSystemUser user, String accountNumber)
			throws PaymentSystemException {

		Account accountfound = getByNumber(accountNumber);
		User userw = (User) user;
		userw.setAccount(accountfound);
		return (userService.save(userw)).getAccount();
	}

	public PaymentSystemAccount addUserToAccount(String emailAddress, String accountNumber)
			throws PaymentSystemException {
		return addUserToAccount(userService.getUserByEmail(emailAddress), accountNumber);
	}

	@Override
	public Iterator<PaymentSystemAccount> getAllAccounts() {
		return accountRepository.findAll().stream().map((a) -> (PaymentSystemAccount) a).collect(Collectors.toList())
				.iterator();
	}

	@Override
	public PaymentSystemAccount getUserAccount(PaymentSystemUser user) {
		User userRet = userService.getUserByEmail(user.getEmailAddress());
		return userRet.getAccount();
	}

	@Override
	public Iterator<PaymentSystemAccount> findAccountsByFullName(String firstName, String lastName) {
		if (firstName == null || lastName==null)
			return null;
		
		return accountRepository.findAccountsByFullName(firstName, lastName).stream()
				.map((a) -> (PaymentSystemAccount) a).collect(Collectors.toList()).iterator();
	}

	@Override
	public Iterator<PaymentSystemAccount> findAccountsByFirstName(String firstName) {
		if (firstName == null)
			return null;
		
		return accountRepository.findByFirstName(firstName).stream().map((a) -> (PaymentSystemAccount) a)
				.collect(Collectors.toList()).iterator();
	}

	@Override
	public Iterator<PaymentSystemAccount> findAccountsByLastName(String lastName) {
		if (lastName == null)
			return null;
		return accountRepository.findByLastName(lastName).stream().map((a) -> (PaymentSystemAccount) a)
				.collect(Collectors.toList()).iterator();
	}

	/**
	 * Persist several accounts
	 * 
	 * @param accounts
	 */
	public Account save(Account account) {
		return accountRepository.save(account);
	}
	
	
	public Account getByNumber(String accountNumber) {
		Optional<Account> accountfound = accountRepository.findByAccountNumber(accountNumber);
		accountfound.orElseThrow(() -> new NoSuchElementException("accountNumber not found:" + accountNumber));
		return accountfound.get();
	}
	
	public List<AvgAccountDto> averageByCustomer() {
		return accountRepository.averageCustomer();
	}
}
