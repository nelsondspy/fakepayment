package com.paypal.fakepayment.impl.account;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.impl.user.User;

import lombok.Data;
import lombok.Getter;

@Entity
@Data
public class Account implements PaymentSystemAccount {
	@Id
	@GeneratedValue
	@JsonIgnore
	private long id;
	
	
	@Column(name = "number")
	private String accountNumber;
	
	/**
	 * Usually BigDecimal is used  
	 */
	@Column(name = "balance")
	private Double accountBalance;
	
	@Getter
	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<User> accountUsersList;


	@Override
	public void incrementAccountBalance(Double amount) throws PaymentSystemException {
		setAccountBalance(getAccountBalance() + amount );
	}

	@Override
	public void decrementAccountBalance(Double amount) throws PaymentSystemException {
		if (amount > this.accountBalance )
				throw new PaymentSystemException("Amount is greater than balance");
		
		setAccountBalance(getAccountBalance() - amount );
	}
	
	/**
	 * Doesn't change the balance, just throws a exception if balance is not enough    
	 * @param amount
	 * @throws PaymentSystemException
	 */
	public void tryDecrementAccountBalance(Double amount) throws PaymentSystemException {
		if (amount > this.accountBalance )
				throw new PaymentSystemException("Amount is greater than balance");
		
	}

	@Override
	public Iterator<PaymentSystemUser> getAccountUsers() {
		return getAccountUsersList().stream().map((a) -> (PaymentSystemUser) a).collect(Collectors.toList()).iterator();
	}

}
