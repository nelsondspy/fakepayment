package com.paypal.fakepayment.impl.account;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	
	  Optional<Account> findByAccountNumber(String number);
	  
	  @Query ("select u.account from User u where u.firstName like ?1")
	  List<Account> findByFirstName(String firstName);

	  @Query ("select u.account from User u where u.lastName like ?1")
	  List<Account> findByLastName(String lastname);

	  @Query ("select u.account from User u where u.firstName like ?1 and u.lastName like ?2")
	  List<Account> findAccountsByFullName(String firstName, String lastname);	  
	  
		//TODO I would prefer implement firstly in native sql, analyze and then convert to jpql  
		@Query("select new com.paypal.fakepayment.impl.account.AvgAccountDto (u.account.accountNumber , "
				+ "count(u.account.id),u.account.accountBalance / count(u.account.id)) "
				+ "from User as u group by u.account.accountNumber, u.account.accountBalance ")
		public List<AvgAccountDto> averageCustomer();

	  
}
