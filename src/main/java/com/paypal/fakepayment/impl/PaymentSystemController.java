package com.paypal.fakepayment.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paypal.fakepayment.PaymentSystemAccount;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.impl.account.AccountManagerService;
import com.paypal.fakepayment.impl.account.AvgAccountDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;


@Api("Endpoint to handle operations in the Fake Payment System")
@RestController
@RequestMapping("api/fakepay")
@AllArgsConstructor
public class PaymentSystemController {
	
	private final PaymentSystemService paymentSysService;
	private final AccountManagerService accountService;
	 
	
	@PostMapping("sendmoney")
	@ApiOperation(value = "send money from an account to a account list")
	public void sendMoney(@RequestBody SendMoneyDto data) throws PaymentSystemException {
		paymentSysService.sendMoney(data.getEmailFrom(), data.getEmailListTo(), data.getAmount());
	}
	
	
	@PostMapping("distribute")
	@ApiOperation(value = "slice the amount in equal parts and sends, from an account to a account list")
	public void distributeMoney(@RequestBody SendMoneyDto data) throws PaymentSystemException {
		paymentSysService.distributeMoney(data.getEmailFrom(), data.getEmailListTo(), data.getAmount());
	}
	
	
	@GetMapping("customerlist")
	public Iterator<PaymentSystemAccount> getCustomerList() {
		return paymentSysService.getAccountManager().getAllAccounts() ;
	}

	@GetMapping("customerkpi")
	@ApiOperation(value = "list average of money per user and count, for each account ")
	public List<AvgAccountDto> getCustomerKpi() {
		return accountService.averageByCustomer();
	}	
	
	@Data
	public final static class SendMoneyDto{
		private String emailFrom;
		private Set<String >emailListTo;
		private double amount ; 
	} 
	
}
