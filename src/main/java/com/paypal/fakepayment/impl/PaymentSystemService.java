package com.paypal.fakepayment.impl;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import com.paypal.fakepayment.PaymentSystem;
import com.paypal.fakepayment.PaymentSystemAccountManager;
import com.paypal.fakepayment.PaymentSystemException;
import com.paypal.fakepayment.PaymentSystemUser;
import com.paypal.fakepayment.PaymentSystemUserManager;
import com.paypal.fakepayment.impl.account.Account;
import com.paypal.fakepayment.impl.account.AccountManagerService;
import com.paypal.fakepayment.impl.user.User;
import com.paypal.fakepayment.impl.user.UserManagerService;

import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class PaymentSystemService implements PaymentSystem {
	
	private final AccountManagerService accountService; 
	private final UserManagerService userService;
	
	@Override
	public PaymentSystemAccountManager getAccountManager() {
		return accountService;
	}

	@Override
	public PaymentSystemUserManager getUserManager() {
		return userService;
	}

	
	@Override
	public void sendMoney(PaymentSystemUser from, PaymentSystemUser to, double amount) throws PaymentSystemException {

		Account fromAccount = (Account) accountService.getUserAccount(from);
		Account toAccount = (Account) accountService.getUserAccount(to);
		fromAccount.decrementAccountBalance(amount);
		toAccount.incrementAccountBalance(amount);

		accountService.save(fromAccount);
		accountService.save(toAccount);
	}

	
	@Override
	public void sendMoney(PaymentSystemUser from, Set<PaymentSystemUser> to, double amount)
			throws PaymentSystemException {
		
		double totalToDecrement = amount * to.size();
		
		// Throws exception If account balance is insufficient
		Account fromAccount = ((User) from).getAccount();
		fromAccount.tryDecrementAccountBalance(totalToDecrement);
		for (PaymentSystemUser toAccount : to) {
			sendMoney( from, toAccount, amount);  
		}
		
	}

	@Override
	public void distributeMoney(PaymentSystemUser from, Set<PaymentSystemUser> to, double amount)
			throws PaymentSystemException {
		double amountPerToAccount = amount / to.size();
		sendMoney(from, to, amountPerToAccount);

	}
	
	
	/**
	 * Perform send operation, using email addresses   
	 * @param fromUserEmail user's email address who is sending the amount   
	 * @param toEmailList
	 * @param amount
	 * @throws PaymentSystemException
	 */
	public void sendMoney(String fromUserEmail, Set<String> toEmailList, double amount) throws PaymentSystemException {
		User userFrom = userService.getUserByEmail(fromUserEmail);
		
		// Verify if each email exists
		Set<PaymentSystemUser> toUsers = toEmailList.stream().map((e) -> (PaymentSystemUser)userService.getUserByEmail(e))
				.collect(Collectors.toSet());
		sendMoney((PaymentSystemUser) userFrom, toUsers, amount);
	}	
	
	
	/**
	 * Perform distribute operation, using email addresses   
	 * @param fromUserEmail
	 * @param toEmailList
	 * @param amount
	 * @throws PaymentSystemException
	 */
	public void distributeMoney(String fromUserEmail, Set<String> toEmailList, double amount)
			throws PaymentSystemException {
		User userFrom = userService.getUserByEmail(fromUserEmail);
		// Verify if each email exists
		Set<PaymentSystemUser> toUsers = toEmailList.stream().map((e) -> userService.getUserByEmail(e))
				.collect(Collectors.toSet());
		distributeMoney((PaymentSystemUser) userFrom, toUsers, amount);
	}
	
	

}
