package com.paypal.fakepayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
public class FakepaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FakepaymentApplication.class, args);
	}
	@Bean
	public WebMvcConfigurer adapter() {
		return new WebMvcConfigurer() {


			/**
			 * This configures the CORS.
			 *
			 * In this configuration anyone can call our API (it'se secury with the usage of
			 * headers).
			 *
			 * @param registry
			 *            the registry to configure
			 */
			@Override
			public void addCorsMappings(CorsRegistry registry) {

				registry.addMapping("/**")
						.allowedOrigins("*")
						.allowedMethods("GET", "OPTIONS", "POST", "PUT", "DELETE");
			}
		};
	}

}
