-- Name: hibernate_sequence; Type: SEQUENCE
CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.account (
    id bigint NOT NULL,
	number text NOT NULL,
	balance float8,
	CONSTRAINT account_pkey PRIMARY KEY (id),
 	CONSTRAINT account_uniq UNIQUE (number)
);


CREATE TABLE public."user" (
    id bigint NOT NULL,
    first_name text,
    last_name text,
    email_address text,
    create_date timestamp without time zone,
    account_id bigint, 
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT user_uniqemail UNIQUE (email_address),
    CONSTRAINT "user_account_ref" FOREIGN KEY (account_id) REFERENCES public.account (id)
);
