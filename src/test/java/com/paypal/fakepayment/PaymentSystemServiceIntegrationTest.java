package com.paypal.fakepayment;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.paypal.fakepayment.impl.PaymentSystemService;
import com.paypal.fakepayment.impl.account.Account;
import com.paypal.fakepayment.impl.account.AccountManagerService;
import com.paypal.fakepayment.impl.user.UserManagerService;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-int-test.properties")

class PaymentSystemServiceIntegrationTest {

	@Autowired
	private PaymentSystemService paymentSystemService;

	@Autowired
	private AccountManagerService accountService;

	@Autowired
	private UserManagerService userManageService;

	@Test
	public void testHappyPath() throws PaymentSystemException {

		PaymentSystem paymentSystem = paymentSystemService;

		Assert.assertNull(paymentSystem.getAccountManager().findAccountsByFirstName(null));

		Assert.assertNotNull(paymentSystem.getAccountManager().findAccountsByFirstName(""));

		Assert.assertFalse(paymentSystem.getAccountManager().findAccountsByFirstName("").hasNext());

		Assert.assertNotNull(paymentSystem.getAccountManager().findAccountsByLastName(""));

		Assert.assertNull(paymentSystem.getAccountManager().findAccountsByLastName(null));

		Assert.assertFalse(paymentSystem.getAccountManager().findAccountsByLastName("").hasNext());

		Assert.assertNotNull(paymentSystem.getAccountManager().findAccountsByFullName("", ""));

		Assert.assertNull(paymentSystem.getAccountManager().findAccountsByFullName(null, ""));
		Assert.assertNull(paymentSystem.getAccountManager().findAccountsByFullName("", null));
		Assert.assertFalse(paymentSystem.getAccountManager().findAccountsByFullName("", "").hasNext());

		PaymentSystemUser user1 = paymentSystem.getUserManager().createUser("John", "Doe", "john.doe@host.net");

		PaymentSystemUser user2 = paymentSystem.getUserManager().createUser("Jane", "Doe", "jane.doe@host.net");
		PaymentSystemUser user3 = paymentSystem.getUserManager().createUser("Gene", "Smith", "gene.smith@host.net");
		PaymentSystemUser user4 = paymentSystem.getUserManager().createUser("John", "Johnson", "john.Johnson@host.net");

		PaymentSystemAccount account1 = paymentSystem.getAccountManager().createAccount(user1, 10);
		PaymentSystemAccount account2 = paymentSystem.getAccountManager().createAccount(user3, 0);
		PaymentSystemAccount account3 = paymentSystem.getAccountManager().createAccount(user4, 0);

		Assert.assertNotNull(account1);
		Assert.assertNotNull(account2);
		Assert.assertNotNull(account3);
		Assert.assertEquals(Double.valueOf(10), account1.getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), account2.getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), account3.getAccountBalance());

		paymentSystem.getAccountManager().addUserToAccount(user2, account1.getAccountNumber());

		int numAccounts = 0;
		for (Iterator<PaymentSystemAccount> allAccounts = paymentSystem.getAccountManager()
				.getAllAccounts(); allAccounts.hasNext(); allAccounts.next()) {
			numAccounts++;
		}
		Assert.assertEquals(3, numAccounts);

		Assert.assertEquals(account1.getAccountNumber(),
				paymentSystem.getAccountManager().getUserAccount(user1).getAccountNumber());
		Assert.assertEquals(account1.getAccountNumber(),
				paymentSystem.getAccountManager().getUserAccount(user2).getAccountNumber());
		Assert.assertEquals(account2.getAccountNumber(),
				paymentSystem.getAccountManager().getUserAccount(user3).getAccountNumber());
		Assert.assertEquals(account3.getAccountNumber(),
				paymentSystem.getAccountManager().getUserAccount(user4).getAccountNumber());

		paymentSystem.sendMoney(user1, user3, 5);

		Assert.assertEquals(Double.valueOf(5), getUpdatedAccount(account1).getAccountBalance());
		Assert.assertEquals(Double.valueOf(5), getUpdatedAccount(account2).getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account3).getAccountBalance());

		paymentSystem.sendMoney(user2, user3, 5);

		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account1).getAccountBalance());
		Assert.assertEquals(Double.valueOf(10), getUpdatedAccount(account2).getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account3).getAccountBalance());

		Set<PaymentSystemUser> to = new HashSet<PaymentSystemUser>();
		to.add(getUpdatedUser(user1));
		to.add(getUpdatedUser(user2));

		paymentSystem.sendMoney(getUpdatedUser(user3), to, 5);

		Assert.assertEquals(Double.valueOf(10), getUpdatedAccount(account1).getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account2).getAccountBalance());
		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account3).getAccountBalance());

		to.clear();
		to.add(getUpdatedUser(user3));
		to.add(getUpdatedUser(user4));

		paymentSystem.distributeMoney(getUpdatedUser(user2), to, 10);

		Assert.assertEquals(Double.valueOf(0), getUpdatedAccount(account1).getAccountBalance());
		Assert.assertEquals(Double.valueOf(5), getUpdatedAccount(account2).getAccountBalance());
		Assert.assertEquals(Double.valueOf(5), getUpdatedAccount(account3).getAccountBalance());

	}

	protected Account getUpdatedAccount(PaymentSystemAccount account1) {
		return accountService.getByNumber(account1.getAccountNumber());
	}

	protected PaymentSystemUser getUpdatedUser(PaymentSystemUser user) {
		return userManageService.getUserByEmail(user.getEmailAddress());
	}
}
