FROM openjdk:8-jre
MAINTAINER Nelson Duranona <nelsonds.py@gmail.com>

VOLUME /tmp
COPY /target/*SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

